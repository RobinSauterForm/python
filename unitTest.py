#Tests
import re

class Correction:
    def make_low_cap(self, s):
        """
        >>> c = Correction ()
        >>> c.make_low_cap("I will be Back !")
        'i will be back !'
        """
        return s.lower()
    
    def crypt_tel(self, s):
        """
        >>> c = Correction ()
        >>> c.crypt_tel("42069 Half Life 3")
        '*** Half Life ***'
        """
        return re.sub("[0-9]+","***", s)

if __name__ == "__main__":

    import doctest
    doctest.testmod()

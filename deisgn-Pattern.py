# Design Pattern GoF "Composite"

class Sonde:

    def nbSonde(self):
        return 1

class SondeSimple(Sonde):

    def __init__(self, value):
        self.value = value

    def display(self):
        print(self.value)

    def ValueMax(self):
        return self.value

class VecSonde(Sonde):

    def __init__(self, sondes):
        self.sondes = sondes

    def display(self):
        for s in self.sondes:
            s.display()

    def ValueMax(self):
        sMax = -999999
        for s in self.sondes:
            if s.ValueMax() > sMax:
                sMax = s.ValueMax()
        return sMax

    def nbSonde(self):
        nb = 1
        for s in self.sondes:
           nb += s.nbSonde()
        return nb

    def moyValue(self):
        moy = 0
        for s in self.sondes:
            moy += s.ValueMax()
        moy /= (self.nbSonde() - len(self.sondes))
        return moy



s1 = SondeSimple(2)
s2 = SondeSimple(99)
vS = VecSonde([s1, s2])
s3 = SondeSimple(12)
vS2 = VecSonde([s3, vS])
vS2.display()

print(vS2.ValueMax())
print(vS2.nbSonde())
print(vS2.moyValue())

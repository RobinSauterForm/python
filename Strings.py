

textes = {
    "La mort n'est que le commencement, je suis devenu la mort, le destructeur des mondes.",
    "They called me a madman. And what I predicted came to pass.",
    "The hardest choices require the strongest wills."
    }

stats = {
    "fr":{'w':0, 'y':0.1, 'u':6, 'q':1.4 , 'h':0.6},
    "en":{'w':2, 'y':2, 'u':2.7, 'q':0.1 , 'h':6}
    }

compLetters = ('w','q','y','u','h')

for txt in textes:

    nb = {}

    for letter in compLetters:
    
        nb[letter] = 0

    for letter in txt.lower():
        if letter in compLetters:
            nb[letter] += 1 / len(txt.replace(' ', ''))

  
    nbEn = 0
    nbFr = 0
    for lang in stats:
        
        for letter in stats[lang]:

            for langComp in stats:
                
                if langComp != lang:
                    
                    if(stats[langComp][letter] > stats[lang][letter]):
                
                        if nb[letter] > stats[lang][letter]:
                            nbEn += 1
                        else:
                            nbFr += 1

                    else:

                        if nb[letter] < stats[lang][letter]:
                            nbEn += 1
                        else:
                            nbFr += 1

    if nbEn > nbFr:
        print("En")
    else:
        print("Fr")
    print("fin Texte")       

##    nbY = 0
##    for letter in txt:
##        if letter == 'y' or letter == 'Y':
##            nbY += 1
##    nbY /= len(txt)
##
##    for lang in stats:
##        for letter in lang:
##            if nbY > letter:
##                print("en")
##            else:
##                print("fr")


for txt in textes:
    valid = True    
    for letter in range(len(txt) - 1):
        if(txt[letter] == ',') and (txt[letter] != ' '):
            valid = False
    print("Valid typo : " + str(valid))

for txt in textes:
    word = "Text" in txt or "mot" in txt or "word" in txt
    print("Word find : " + str(word))

    

import re

# if word < 9 let ; excl: - ; "

for txt in textes:
    
    simple = re.match(".*[A-Za-z]{10}", txt) == None
    if simple:
        simple = re.match("[;\"\-]", txt) == None
    print("Simple : " + str(simple))
    


    





class Capteur:

    def __init__(
                    self,
                    name = "Default Capteur",
                    minValue = -9999999,
                    maxValue  = 9999999,
                    currentValue = 0
                ):

        self.name = name
        self.minValue = minValue
        self.maxValue = maxValue
        self.currentValue = currentValue
         
        
    def __add__(self, addValue):
        dummy = Capteur(self.name,  self.minValue, self.maxValue)
        if (self.currentValue + addValue) < self.maxValue:
            dummy.currentValue = self.currentValue + addValue
            return dummy
            
    def __mul__(self, mulValue):
        dummy = Capteur(
                            self.name,
                            self.minValue * mulValue,
                            self.maxValue * mulValue,
                            self.currentValue * mulValue
                        )
        return dummy

    def __truediv__(self, divValue):
       return self * (1/divValue)


    def display(self):
        print  (
                    self.name +
                    " , " + str(self.minValue) +
                    " , " + str(self.maxValue) +
                    " , " + str(self.currentValue)
                )

    def VisualDisplay(self):
        str = self.name + " |"
        for n in range(10):
            if  (
                    n <
                    (
                        (self.currentValue - self.minValue)
                        * 10 / (self.maxValue - self.minValue)
                    )
                ):
                str += "#"
            else:
                str += "-"
        str += "|"
        print(str)

    def __del__(self):
        print("Pepsi? Pepsi! lifen't...")


class DoubleCapteur(Capteur):
    def __init__(
                    self,
                    name = "Default Capteur",
                    minValue = -9999999,
                    maxValue  = 9999999,
                    currentValue = 0,
                    currentValue2 = 0
                ):
        Capteur.__init__(self, name, minValue, maxValue, currentValue)
        if currentValue2 >= currentValue2:
            self.currentValue2 = currentValue2
        else:
            self.currentValue2 = currentValue

    def display(self):
        Capteur.display(self)
        print  (
                    " , " + str(self.currentValue2)
                )

   
a = Capteur()
a.display()
a.VisualDisplay()


b = Capteur("Capteur 2", 0, 100, 25)
b.display()
b.VisualDisplay()
b *= 2
b /= 4
b.display()
b += 5
b.display()

c = DoubleCapteur("Capteur 3", 0, 100, 25, 20)
c.display()
    

#! /usr/bin/env python
#coding: utf8

capteur1 = {"F","B","H"}
capteur2 = {"A","F"}
capteur3 = {"F","E","A"}

signaux = [capteur1, capteur2, capteur3]

#check all active
cap1 = capteur1
for cap in signaux[1:]:
    cap1 = cap1 & cap
print("Sig On on all cap")
print(cap1)

#check all if 1 active
cap2 = capteur1
for cap in signaux[1:]:
    cap2 = cap2 | cap
print("All sig")
print(cap2)


#check all if active imp
cap3 = capteur1
for cap in signaux[1:]:
    cap3 = cap3 ^ cap
print("All sig imp")
print(cap3)

cap4 = cap2 - cap3
print("All sig p")
print(cap4)

difSig={}
for cap in signaux:
    for sym in cap:
        if(sym in difSig):
            difSig[sym] += 1
        else:
            difSig[sym] = 1
print("All sig + count")
print(difSig)

DualCoreAllTheThings = {
    "signaux" : signaux,
    "results" : {
        "1:" : cap1,
        "2:" : cap2,
        "3:" : cap3,
        "4:" : cap4,
        "5:" : difSig
    }
}
print("Hack of the Things " + str(DualCoreAllTheThings))
    

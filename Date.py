import datetime as t
import re


log = """
0001-01-01 11:55:33 Imperator Rome
0867-01-01 11:33:33 CK3
1444-11-01 11:22:33 EU4
1936-01-01 11:11:33 HOI4
2021-03-01 11:44:33 Surviving Mars
2300-01-01 11:44:33 Stellaris
"""

for line in log.splitlines():
    
    if line == "": continue
    
    sYear = int(line[0:4])
    sMonth = int(line[5:7])
    sDay = int(line[8:10])
    sH = int(line[11:13])
    sM = int(line[14:16])
    sS = int(line[17:19])
    
    ti = t.datetime(sYear,sMonth,sDay,sH,sM,sS)
    
    now = t.datetime.now()

    print("log :", line[20:], end =",")
    if (now.year - ti.year) > 0:
        print(str(now.year - ti.year) + " years ago", end ="," )
    if (now.month - ti.month) > 0:
        print(str(now.month - ti.month) + " months ago", end ="," )
    if (now.day - ti.day) > 0:     
        print(str(now.day - ti.day) + " days ago")
    # d,m,s 
    
